package com.udn.userconfirmation.test.service;

import java.util.UUID;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.udn.userconfirmation.model.User;
import com.udn.userconfirmation.service.ConfirmType;
import com.udn.userconfirmation.service.OpenupUserConfirmer;
import com.udn.userconfirmation.service.TokenGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-context.xml")
@WebAppConfiguration
public class OpenupUserConfirmerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(OpenupUserConfirmerTest.class);

	@Autowired
	OpenupUserConfirmer userConfirmer;
	
	@Autowired 
	TokenGenerator generator;
	
	@Test
	public void testSimpleEndorseMail() throws Exception {
		User user = new User();
		user.setEmail("phil.li@udngroup.com.tw");
		user.setName("小李子");
		user.setId(generator.getEmailToken());
		user.setToken(generator.getEmailToken());
		
		userConfirmer.sendMailForConfirmation(ConfirmType.Endorse, user);
	}
	
	@Test
	public void testSimpleProposeMail() throws Exception {
		User user = new User();
		user.setEmail("phil.li@udngroup.com.tw");
		user.setName("小李子");
		user.setId(generator.getEmailToken());
		user.setToken(generator.getEmailToken());
		
		userConfirmer.sendMailForConfirmation(ConfirmType.Propose, user);
	}
	
	@Test
	public void testSimpleSms() throws Exception {
		User user = new User();
		user.setName("王大明");
		user.setMobile("0905512215");
		user.setToken(generator.getMobileToken());

		userConfirmer.sendSmsForConfirmation(user);
	}
	
	@Test
	public void testNo() {
		logger.debug("name: {}, string: {}", ConfirmType.Endorse.name(), ConfirmType.Endorse.toString());
	}
}
