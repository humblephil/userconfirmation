package com.udn.userconfirmation.test.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-context.xml")
@WebAppConfiguration
public class HomeControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeControllerTest.class);

	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext webContext;
	
	@Before
	public void setup()  {
		logger.debug("Create mock mvc.");
		this.mvc = MockMvcBuilders.webAppContextSetup(webContext).build();
	}
	
	@Test
	public void testGetHomeController_for_normal_home_request() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/"))
			.andExpect(status().isOk())
			.andExpect(view().name("home"))
			.andExpect(forwardedUrl("/WEB-INF/views/home.jsp"));
	}
	
	@Test
	public void testGetHomeController_for_bad_request() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/dummy_should_not_found"))
			.andExpect(status().isNotFound());
	}
}
