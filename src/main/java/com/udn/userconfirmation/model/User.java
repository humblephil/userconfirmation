package com.udn.userconfirmation.model;

import java.util.Date;

public class User {

	private String id;
	private String name;
	private String ideaId;
	private Boolean isConfirmed;
	private Date issuingTime;
	private long validSpanSeconds;
	private String token;
	private String email;
	private String mobile;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}
	public Boolean getIsConfirmed() {
		return isConfirmed;
	}
	public void setIsConfirmed(Boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
	public Date getIssuingTime() {
		return issuingTime;
	}
	public void setIssuingTime(Date issuingTime) {
		this.issuingTime = issuingTime;
	}
	public long getValidSpanSeconds() {
		return validSpanSeconds;
	}
	public void setValidSpanSeconds(long validSpanSeconds) {
		this.validSpanSeconds = validSpanSeconds;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
