package com.udn.userconfirmation.service;

public enum ConfirmType {
	Propose,
	Endorse
}
