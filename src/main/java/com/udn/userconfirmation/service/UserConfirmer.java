package com.udn.userconfirmation.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.udn.userconfirmation.model.User;

public interface UserConfirmer {

	void sendMailForConfirmation(ConfirmType type, User user);
	void sendSmsForConfirmation(User user);
	
	boolean confirmMail(User expectedUser, String userName, String token);
	boolean confirmSms(String token);
}
