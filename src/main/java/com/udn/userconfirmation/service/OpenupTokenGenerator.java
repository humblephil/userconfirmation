package com.udn.userconfirmation.service;

import java.util.Random;
import java.util.UUID;

public class OpenupTokenGenerator implements TokenGenerator {

	@Override
	public String getEmailToken() {
		return UUID.randomUUID().toString();
	}

	@Override
	public String getMobileToken() {
		Random randomizer = new Random();
		int value = randomizer.nextInt(999999);
		return String.format("%06d",  value);
	}

}
