package com.udn.userconfirmation.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.mashape.unirest.http.Unirest;
import com.udn.userconfirmation.model.User;

@Service
public class OpenupUserConfirmer implements UserConfirmer {
	
	private static final Logger logger = LoggerFactory.getLogger(OpenupUserConfirmer.class);

	@Resource(name = "mailerProperties")
	Map<String, String> mailerProperties;
	
	@Resource(name = "smsProperties")
	Map<String, String> smsProperties;
	
	@Autowired
	private JavaMailSender mailer;
	
	@Autowired
	private VelocityEngine engine;

	@Override
	@Async
	public void sendMailForConfirmation(ConfirmType type, User user) {
		String templateKey = String.format("%sTemplate", type.name().toLowerCase());
		String subjectKey = String.format("%sSubject", type.name().toLowerCase());
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		String emailText = VelocityEngineUtils.mergeTemplateIntoString(
			engine, mailerProperties.get(templateKey), "utf-8", model);
		
		MimeMessage message = mailer.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(user.getEmail());
			helper.setSubject(mailerProperties.get(subjectKey));
			helper.setText(emailText, true);
			mailer.send(message);
		} 
		catch (MessagingException e) {
			logger.warn("Send mail for {} fail: {}", type.name(), e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void sendSmsForConfirmation(User user) {
		try {			
			int status = Unirest.get(smsProperties.get("service"))
				.queryString("username", smsProperties.get("username"))
				.queryString("password", smsProperties.get("password"))
				.queryString("dstaddr", user.getMobile())
				.queryString("encoding", "UTF8")
				.queryString("smbody", String.format(smsProperties.get("smsTemplate"), user.getName(), user.getToken()))
				.asString()
				.getStatus();
			if (status != 200) {
				logger.warn("SMS failed to send to {}. Status: {}", user.getName(), status);
			}
		}
		catch (Exception e) {
			logger.warn("SMS failed to send to {}: {}", user.getName(), e.getMessage());
		}
	}

	@Override
	public boolean confirmMail(User expectedUser, String userName, String token) {
		
		return false;
	}

	@Override
	public boolean confirmSms(String token) {
		// TODO Auto-generated method stub
		return false;
	}

}
