package com.udn.userconfirmation.service;

public interface TokenGenerator {
	
	String getEmailToken();
	String getMobileToken();
}
